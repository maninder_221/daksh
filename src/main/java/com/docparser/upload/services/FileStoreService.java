package com.docparser.upload.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.docparser.upload.entity.UploadDocumentRecord;
import com.docparser.upload.exception.FileNotPresentinDirectory;
import com.docparser.upload.exception.FileRecordNotFound;
import com.docparser.upload.property.FileStorageProperty;

@Service
public class FileStoreService {
	
	private final Path locationOfUploadFile;
	
	@Autowired
	private UploadDocumentService udservice;
	
	@Autowired
	public FileStoreService(FileStorageProperty filestoreproperty) {
		this.locationOfUploadFile=Paths.get(filestoreproperty.getUploadDir()).toAbsolutePath().normalize();
	
	
	try {
		Files.createDirectories(locationOfUploadFile);
	}
	catch(Exception e)
	{
		
	}
}
	
	public String storeFile(MultipartFile file) {
		String fileName= StringUtils.cleanPath(file.getOriginalFilename());
		String fileExtension=FilenameUtils.getExtension(fileName);
		
		 try {
	            if(fileName.contains("..")) {
	                throw new RuntimeException("filename is appropriate");
	            }
	            if(!fileExtension.equals("pdf")) {
	            	throw new RuntimeException("File should be a PDF.");
	            }else {
	            	  UploadDocumentRecord filerecord=udservice.findByFileName(fileName);
			  	            if(filerecord!=null)
			  	            {
			  	            	throw new FileRecordNotFound("File already present in database");
			  	            }
			  	            else {
			  	           
			  	            Path targetLocation = this.locationOfUploadFile.resolve(fileName);
			  	            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
			  	            
			  	        }
	            }
	            return fileName;
	        } catch (IOException ex) {
	            System.out.println("Could not store file " + fileName + ". Please try again!");
	        }
		return fileName;
	}
	
	public Resource downloadFile(String filename) {
		Path filePath= this.locationOfUploadFile.resolve(filename).normalize();
		Resource resource= null;
		try {
			resource= new UrlResource(filePath.toUri());
			if(resource.exists()) {
			return resource;
			}
			
		} catch (MalformedURLException e) {
			throw new FileNotPresentinDirectory("File not found "+filename);
		}
		return resource;
		
	}
	
	
	

}
