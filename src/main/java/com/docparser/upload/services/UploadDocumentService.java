package com.docparser.upload.services;

import com.docparser.upload.entity.UploadDocumentRecord;

public interface UploadDocumentService {

	  public UploadDocumentRecord addNewDocument(UploadDocumentRecord emp);
	  
	  public UploadDocumentRecord findByFileName(String filename);

	  
}
