package com.docparser.upload.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.docparser.upload.entity.UploadDocumentRecord;
import com.docparser.upload.repository.UploadDocumentRepository;

@Service
public class UploadDocumentServiceImpl implements UploadDocumentService {

	@Autowired
	UploadDocumentRepository uploadDocRepository;
	@Override
	public UploadDocumentRecord addNewDocument(UploadDocumentRecord udr) {
		
		return uploadDocRepository.save(udr);
	}
	@Override
	public UploadDocumentRecord findByFileName(String filename) {
		
		return uploadDocRepository.findByFileName(filename);
	}
	

}
