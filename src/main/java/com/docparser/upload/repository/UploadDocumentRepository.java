package com.docparser.upload.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.docparser.upload.entity.UploadDocumentRecord;
@Repository
public interface UploadDocumentRepository  extends JpaRepository<UploadDocumentRecord,Long> {

	public UploadDocumentRecord findByFileName(String filename);
}
