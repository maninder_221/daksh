package com.docparser.upload.resources;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.docparser.upload.entity.ResponseClass;
import com.docparser.upload.entity.UploadDocumentRecord;
import com.docparser.upload.services.FileStoreService;
import com.docparser.upload.services.UploadDocumentService;

@RestController
@RequestMapping("/api")
public class FileUploadController {

	@Autowired
	private FileStoreService fileStoreService;
	
	@Autowired
	private UploadDocumentService udservice;
	
	@GetMapping("/welcome")
	public String index() {
        return "index";
    }
	
	@PostMapping("/upload")
	public ResponseClass uploadFile(@RequestParam("file") MultipartFile file) {
		String fileName= fileStoreService.storeFile(file);
		
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/downloadfile/")
                .path(fileName)
                .toUriString();
		String fileViewingUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/api/getpdf/")
                .path(fileName)
                .toUriString();
		
		UploadDocumentRecord saveRecord= new UploadDocumentRecord(file.getSize(),fileName,fileDownloadUri);
		UploadDocumentRecord saved=udservice.addNewDocument(saveRecord);
		
		
		ResponseClass response= new ResponseClass(fileDownloadUri,fileName,saved,fileViewingUri);
		return response;
	}
	@GetMapping("/downloadfile/{filename:.+}")
	public ResponseEntity<Resource> downloadFile(@PathVariable("filename") String filename,
			HttpServletRequest request){
		
		Resource resource= fileStoreService.downloadFile(filename);
		String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            //logger.info("Could not determine file type.");
        }
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
		
	}
	
	  @RequestMapping(value="/getpdf/{filename}", method=RequestMethod.GET)
	  public ResponseEntity<byte[]> getPDF1(@PathVariable("filename") String filename) {
	  
		  
		  HttpHeaders headers = new HttpHeaders();
		  
		  headers.setContentType(MediaType.parseMediaType("application/pdf")); //String
		
		  final String uri ="http://localhost:8080/api/downloadfile/"+filename;
		  
		  RestTemplate restTemplate = new RestTemplate();
		  ByteArrayResource result =restTemplate.getForObject(uri, ByteArrayResource.class);
		  
		  byte[] pdf1Bytes=result.getByteArray();
		  headers.add("content-disposition", "inline;filename=" + filename);
		  
		  headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		  
		  ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(pdf1Bytes,
		  headers, HttpStatus.OK);
		  return response; 
		  
	  }
	/*
	 * @GetMapping("/ocr/{filename}") public ResponseEntity<String>
	 * getFileOCR(@PathVariable("filename") String filename){
	 * 
	 * UploadDocumentRecord getDocument= udservice.findByFileName(filename);
	 * 
	 * if(getDocument!=null) { final String uri
	 * ="http://localhost:8080/api/downloadfile/"+filename;
	 * 
	 * RestTemplate restTemplate = new RestTemplate(); ByteArrayResource result
	 * =restTemplate.getForObject(uri, ByteArrayResource.class); File file= new
	 * File(result.getByteArray());
	 * 
	 * }
	 * 
	 * 
	 * return null; }
	 */	 
}
