package com.docparser.upload.resources;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/front")
public class FrontController {
	
	@GetMapping("/page")
    public String index() {
		
        return "index";
    }
	/*
	 * @GetMapping("/secondPage") public String second(Model model) {
	 * 
	 * model.addAttribute(attributeValue) return null; }
	 */
}
