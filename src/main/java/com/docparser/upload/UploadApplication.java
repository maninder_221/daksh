package com.docparser.upload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.docparser.upload.property.FileStorageProperty;

@SpringBootApplication
@EnableConfigurationProperties({
	FileStorageProperty.class
})
public class UploadApplication {

	public static void main(String[] args) {
		SpringApplication.run(UploadApplication.class, args);
	}

}
