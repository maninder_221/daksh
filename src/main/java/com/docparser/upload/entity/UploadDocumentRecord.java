package com.docparser.upload.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="upload_document_record")
public class UploadDocumentRecord {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name="file_size")
	private Long fileSize;
	
	@Column(name="file_name")
	private String fileName;
	
	@Column(name="download_path")
	private String downloadPath;
	
	

	public UploadDocumentRecord() {
		
	}

	public UploadDocumentRecord(Long fileSize, String fileName, String downloadPath) {
		
		this.fileSize = fileSize;
		this.fileName = fileName;
		this.downloadPath = downloadPath;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDownloadPath() {
		return downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}
	
	

}
