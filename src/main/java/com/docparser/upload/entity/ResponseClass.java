package com.docparser.upload.entity;

public class ResponseClass {
	
	private String fileuri;
	private String filename;
	private String fileViewUri;
	private UploadDocumentRecord savedRecord;

	
	public ResponseClass(String fileuri, String filename,
			UploadDocumentRecord savedRecord,String fileViewUri) {
		super();
		this.fileuri = fileuri;
		this.filename = filename;
		this.savedRecord=savedRecord;
		this.fileViewUri=fileViewUri;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFileuri() {
		return fileuri;
	}

	public void setFileuri(String fileuri) {
		this.fileuri = fileuri;
	}

	public String getFileViewUri() {
		return fileViewUri;
	}

	public void setFileViewUri(String fileViewUri) {
		this.fileViewUri = fileViewUri;
	}

	public UploadDocumentRecord getSavedRecord() {
		return savedRecord;
	}

	public void setSavedRecord(UploadDocumentRecord savedRecord) {
		this.savedRecord = savedRecord;
	}
	
	

}
