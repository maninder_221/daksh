package com.docparser.upload.exception;

public class FileRecordNotFound extends RuntimeException {

	public FileRecordNotFound() {
		super();
		
	}

	public FileRecordNotFound(String message) {
		super(message);
		
	}

	public FileRecordNotFound(Throwable cause) {
		super(cause);
		
	}
	

}
