package com.docparser.upload.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class FileNotPresentinDirectory extends RuntimeException {

	public FileNotPresentinDirectory() {
		super();
		
	}

	public FileNotPresentinDirectory(String message, Throwable cause) {
		super(message, cause);
		
	}

	public FileNotPresentinDirectory(String message) {
		super(message);
		
	}
	

}
